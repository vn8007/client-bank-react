import React, {useEffect} from "react";
import {connect} from "react-redux";
import Customer from "../components/Customer";
import {Button, Table, TableBody, TableCell, TableHead, TableRow} from "@mui/material";
import {Title} from "@mui/icons-material";
import { Link } from "react-router-dom";
import {customersOperations} from "../store/customers/indexCustomers";


const Customers = ({customers, dispatch}) => {
    useEffect(()=>
    dispatch(customersOperations.getCustomers()),[dispatch])
  return(

      <React.Fragment>
        <Title>Customers Data</Title>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Age</TableCell>
            </TableRow>
          </TableHead>
      <TableBody>
        {customers? customers.map((item) => {
          return <Customer item={item} key={item._id}/>
        }) : <p>no customers</p>}
      </TableBody>
      </Table>
          <Button
            variant="contained"
            component={Link}
            to="/create"
          >
            CREATE CUSTOMER
          </Button>
      </React.Fragment>
  )
}
const mapStateToProps = (state) => {
  return {
    customers: state.customers.data,
  };
};
export default connect(mapStateToProps) (Customers);
