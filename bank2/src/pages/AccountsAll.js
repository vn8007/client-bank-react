import React, {useEffect} from "react";
import {accountsOperations} from "../store/accounts/indexAccounts";
import {Title} from "@mui/icons-material";
import {Table, TableCell, TableHead, TableRow} from "@mui/material";
import Account from "../components/Account";
import {connect} from "react-redux";

const AccountsAll =({dispatch, accounts}) => {
    useEffect(()=>dispatch(accountsOperations.getAccounts()),[dispatch])
    return(
        <React.Fragment>
            <Title>Customer accounts</Title>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Number</TableCell>
                        <TableCell>Currency</TableCell>
                        <TableCell>Balance</TableCell>
                    </TableRow>
                </TableHead>
                {accounts? accounts.map((item) => {
                    return <Account item={item} key={item._id}/>}) : <p>no accounts</p>}
            </Table>
        </React.Fragment>
    )
}
const mapStateToProps = (state) => {
    return {
        accounts: state.accounts.data,

    };
};

export default connect(mapStateToProps)(AccountsAll);