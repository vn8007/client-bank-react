import React, {useEffect} from 'react';
import {connect} from "react-redux";
import Account from "../components/Account";
import CreateAccount from "../components/CreateAccount";
import {useParams} from "react-router-dom";
import {Table, TableCell, TableHead, TableRow} from "@mui/material";
import {Title} from "@mui/icons-material";
import operationsAccounts from "../store/accounts/operationsAccounts";


const Accounts = ({dispatch, accounts}) => {
    const params = useParams();
    useEffect(() => dispatch(operationsAccounts.getCustomerAccounts(params.id)),[dispatch]);
    return (
        <React.Fragment>
            <Title>Customer accounts</Title>
            <Table size="small">
            <TableHead>
                <TableRow>
                    <TableCell>Number</TableCell>
                    <TableCell>Currency</TableCell>
                    <TableCell>Balance</TableCell>
                </TableRow>
            </TableHead>
                {accounts? accounts.map((item) => {
                    return <Account item={item} key={item._id}/>}) : <p>no accounts</p>}
            </Table>
            <CreateAccount id={(params.id)}/>
        </React.Fragment>
    );
}
const mapStateToProps = (state) => {
    return {
        accounts: state.accounts.data,
};
};

export default connect(mapStateToProps)(Accounts);
