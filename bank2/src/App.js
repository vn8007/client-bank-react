import AppRoutes from "./routes/AppRoutes";
import {Route, Routes} from "react-router-dom";
import Main from "./pages/Main";
import Customers from "./pages/Customers";
import Accounts from "./pages/Accounts";
import CreateCustomer from "./components/CreateCustomer";
import React from "react";
import Page404 from "./pages/Page404";
import AccountsAll from "./pages/AccountsAll";
import ChangeAccount from "./components/ChangeAccount";

const App = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<AppRoutes/>}>
          <Route index element={<Main />}/>
          <Route path="customers" element={<Customers />}/>
          <Route path="customers/:id" element={<Accounts />}/>
          <Route path="create" element={<CreateCustomer />}/>
          <Route path="accounts" element={<AccountsAll />}/>
          <Route path="change/:id" element={<ChangeAccount/>}/>
          <Route path="*" element={<Page404 />}/>
          {/*<Route exact path="/loader" element={<Loader />}/>*/}
        </Route>
      </Routes>
    </>
  );
}

export default App;
