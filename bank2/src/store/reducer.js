import { combineReducers } from 'redux';
import accountsReducer from "./accounts/reducerAccounts";
import customersReducer from "./customers/reducerCustomers";


const reducer = combineReducers({
    accounts: accountsReducer,
    customers: customersReducer,
});

export default reducer;
