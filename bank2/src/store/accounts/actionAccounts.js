import types from './typesAccounts';

const setAccountsData = (accountsData) => ({
    type: types.SET_ACCOUNTS_DATA,
    data: accountsData,
});

const setAccountsLoading =(isLoading) =>({
    type: types.SET_ACCOUNTS_LOADING,
    data: isLoading,
});
export default {
    setAccountsData,
    setAccountsLoading,
};