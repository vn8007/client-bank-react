import axios from "axios";
import actionAccounts from "./actionAccounts";

const getAccounts = () => (dispatch) => {
    axios.get(`http://localhost:3000/api/accounts`).then((res) => {
        dispatch(actionAccounts.setAccountsData(res.data));
        dispatch(actionAccounts.setAccountsLoading(false))
    });
}
const postAccount = (accountData) =>(dispatch) =>{
    axios.post("http://localhost:3000/api/accounts", accountData).then((res) => {
        dispatch(actionAccounts.setAccountsData(res.data));
        dispatch(actionAccounts.setAccountsLoading(false));
    });
}
const getCustomerAccounts = (customerId) => (dispatch) => {
    axios.get(`http://localhost:3000/api/accounts/customer=${customerId}`).then((res) => {
        dispatch(actionAccounts.setAccountsData(res.data));
        dispatch(actionAccounts.setAccountsLoading(false))
    });
}
const deleteAccounts = (accountId) => (dispatch) => {
    axios.delete(`http://localhost:3000/api/accounts/${accountId}`).then((res) => {
        dispatch(actionAccounts.setAccountsData(res.data));
        dispatch(actionAccounts.setAccountsLoading(false))
    });
}
const getAccountById = (accountId) => (dispatch) => {
    axios.get(`http://localhost:3000/api/accounts/${accountId}`).then((res) => {
        dispatch(actionAccounts.setAccountsData(res.data));
        dispatch(actionAccounts.setAccountsLoading(false))
    });
}
const updateAccount = (accountData) =>(dispatch) =>{
    axios.put("http://localhost:3000/api/accounts", accountData).then((res) => {
        dispatch(actionAccounts.setAccountsData(res.data));
        dispatch(actionAccounts.setAccountsLoading(false));
    });
}

export default {
    getAccounts,
    postAccount,
    getCustomerAccounts,
    deleteAccounts,
    getAccountById,
    updateAccount,
}