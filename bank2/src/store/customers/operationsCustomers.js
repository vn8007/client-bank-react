import axios from "axios";
import actionCustomers from "./actionCustomers";
import actionAccounts from "../accounts/actionAccounts";

const getCustomers = () => (dispatch) => {
  axios.get("http://localhost:3000/api/customers").then((res) => {
    dispatch(actionCustomers.setCustomersData(res.data));
    dispatch(actionCustomers.setCustomersLoading(false));
  });
}
const postCustomers = (customersData) =>(dispatch) =>{
  axios.post("http://localhost:3000/api/customers", customersData).then((res) => {
    dispatch(actionAccounts.setCustomersData(res.data));
    dispatch(actionAccounts.setCustomersLoading(false));
  });
}
const deleteCustomer = (customerID) => (dispatch) =>{
  axios.delete(`http://localhost:3000/api/customers/${customerID}`).then((res) =>{
    dispatch(actionCustomers.setCustomersData(res.data));
    dispatch(actionCustomers.setCustomersLoading(false));
  })
}

export default {
  getCustomers,
  postCustomers,
  deleteCustomer,
};
