import customersReducer from './reducerCustomers';

export { default as customersOperations } from './operationsCustomers';
export { default as customersActions } from  './actionCustomers';

export default customersReducer;