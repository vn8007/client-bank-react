const SET_CUSTOMERS_DATA = 'bank2/customers/SET_CUSTOMERS_DATA';
const SET_CUSTOMERS_LOADING = 'bank2/customers/SET_CUSTOMERS_LOADING';

export default {
  SET_CUSTOMERS_DATA,
  SET_CUSTOMERS_LOADING
}