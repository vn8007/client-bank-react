import React, {useState} from 'react';
import {Button, Stack, TextField} from "@mui/material";
import {connect} from "react-redux";
import operationsCustomers from "../store/customers/operationsCustomers";

const CreateCustomer = ({dispatch}) => {
  const [name, setname] = useState("");
  const [email, setemail] = useState("");
  const [age, setage] = useState();
  const newCustomer = {};
  newCustomer.name = name;
  newCustomer.email = email;
  newCustomer.age = age;
  const handleChangeName = (event) => {
    setname(event.target.value);
  };
  const handleChangeEmail = (event) => {
    setemail(event.target.value);
  };
  const handleChangeAge = (event) => {
    setage(event.target.value);
  };

  return (
    <Stack direction="row" spacing={2}>
      <TextField
        id="customer-name"
        label="Name"
        variant="outlined"
        value={name}
        onChange={handleChangeName}
      />
      <TextField
        id="customer-email"
        label="Email"
        variant="outlined"
        value={email}
        onChange={handleChangeEmail}
      />
      <TextField
        id="customer-age"
        label="Age"
        variant="outlined"
        value={age}
        onChange={handleChangeAge}
      />
      <Button
        variant="contained"
        component="a"
        href="/customers"
        onClick={()=> {
          console.log(newCustomer);
          dispatch(operationsCustomers.postCustomers(newCustomer));
        }
        }
      >
        CREATE
      </Button>
    </Stack>
  );
};

export default connect()(CreateCustomer);