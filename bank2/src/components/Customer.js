import React from "react";
import {Button, TableCell, TableRow} from "@mui/material";
import {Link} from "react-router-dom";
import DeleteCustomer from "./DeleteCustomer";

const Customer = ({item}) => {
    const { id, name, email, age } = item;
    const expandedAccounts = `/customers/${id}`;

    return(
    <TableRow key={id}>
        <TableCell>{name}</TableCell>
        <TableCell>{email}</TableCell>
        <TableCell>{age}</TableCell>
        <DeleteCustomer id={id}/>
        <Button
            component={Link}
            to={expandedAccounts}
        >ACCOUNTS
        </Button>
    </TableRow>
    )
}
export default Customer;
