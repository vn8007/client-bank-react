import React from "react";
import {Button, MenuItem, Stack, TextField} from "@mui/material";
import {useParams} from "react-router-dom";
import {connect} from "react-redux";
import operationsAccounts from "../store/accounts/operationsAccounts";

const CreateAccount = ({dispatch, accounts, id}) => {
  const params = useParams();
  const currencies = [
    {
      value: 'USD',
      label: '$',
    },
    {
      value: 'EUR',
      label: '€',
    },
    {
      value: 'UAH',
      label: '₴',
    },
    {
      value: 'CHF',
      label: 'Fr',
    },
    {
      value: 'GBP',
      label: '£',
    },

  ];
    const [currency, setCurrency] = React.useState('EUR');
    const [balance, setBalance] = React.useState();

    const handleChange = (event) => {
      setCurrency(event.target.value);
    };
    const handleChangeBalance = (event) =>{
      setBalance(event.target.value);
    };
    const newAccount ={};
      newAccount.currency = currency;
      newAccount.balance = balance;
      newAccount.customer = params.id;
    return (
      <Stack direction="row" spacing={2}>
        <TextField
            id="outlined-basic"
            label="Balance"
            variant="outlined"
            value={balance}
            onChange={handleChangeBalance}
        />
        <div>
          <TextField
            id="outlined-select-currency"
            select
            label="Currency"
            value={currency}
            onChange={handleChange}
          >
            {currencies.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>

        </div>

        <Button onClick={() => {
        dispatch(operationsAccounts.postAccount(newAccount));
        dispatch(operationsAccounts.getCustomerAccounts(id))
        }} variant="contained">
          create
        </Button>
      </Stack>
    )
  }
const mapStateToProps = (state) => {
  return {
    accounts: state.accounts.data,
  };
};
export default connect(mapStateToProps)(CreateAccount);