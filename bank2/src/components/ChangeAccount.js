import React, {useEffect, useState} from 'react';
import {Link, useParams} from "react-router-dom";
import {connect} from "react-redux";
import {accountsOperations} from "../store/accounts/indexAccounts";
import {Button, Stack, TextField} from "@mui/material";
import operationsAccounts from "../store/accounts/operationsAccounts";

const ChangeAccount = ({accounts, dispatch}) => {
    const [summValue, setSummValue] = useState();

  const params = useParams();
  const handleChangeSumm = (event) => {
    setSummValue(event.target.value);
  };
  const withdraw = {};
  withdraw.number = accounts.number;
  withdraw.balance = parseFloat(accounts.balance) - parseFloat(summValue);

  // const expandedAccounts = `/customers/${accounts.customer}`;
  const expandedAccounts = `/customers`;

  useEffect(() => dispatch(accountsOperations.getAccountById(params.id)), [dispatch])
    const topUpAccount = {};
  topUpAccount.number = accounts.number;
  topUpAccount.balance = parseFloat(accounts.balance) + parseFloat(summValue);

    return (
        <div>
            <div>
                <p>NUMBER: {accounts.number}</p>
                <p>BALANCE: {accounts.balance}</p>
            </div>
            <Button
                onClick={()=> {
                    dispatch(accountsOperations.deleteAccounts(accounts.id));
                }}
                component={Link}
                to={expandedAccounts}
            >DELETE ACCOUNT
            </Button>
          <div>
            <Stack direction="row" spacing={2}>

            <TextField
              id="outlined-basic"
              label="Outlined"
              variant="outlined"
              value={summValue}
              onChange={
                handleChangeSumm
              }
            />
          <Button
            variant="contained"
            onClick={() => {
              dispatch(operationsAccounts.updateAccount(topUpAccount));
              dispatch(accountsOperations.getAccountById(accounts.id));
            }
            }
          > TOP UP ACCOUNT
          </Button>
            <Button disabled={(parseFloat(accounts.balance) >= parseFloat(summValue))? false : true}
                    variant="contained"
                    onClick={() => {
                        dispatch(operationsAccounts.updateAccount(withdraw));
                        dispatch(accountsOperations.getAccountById(accounts.id));
                    }
                    }
            > WITHDRAW
            </Button>
            </Stack>

          </div>
        </div>
    );
};
const mapStateToProps = (state) => {
    return {
        accounts: state.accounts.data,
    };
}

export default connect(mapStateToProps)(ChangeAccount);