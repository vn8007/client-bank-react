import React from 'react';
import {Button, TableCell, TableRow} from "@mui/material";
import {Link} from "react-router-dom";

const Account = ({item}) => {
    const {number, balance, currency, id} = item;
    const link = `/change/${id}`;
    return (
        <TableRow key={id}>
            <TableCell>{number}</TableCell>
            <TableCell>{currency}</TableCell>
            <TableCell>{balance}</TableCell>
            <Button
                component={Link}
                to={link}
            >Change Account
            </Button>

        </TableRow>

    );
}


export default Account;