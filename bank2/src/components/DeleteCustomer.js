import React from "react";
import {Button, TableRow} from "@mui/material";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import operationsCustomers from "../store/customers/operationsCustomers";

const DeleteCustomer = ({id, dispatch}) => {
    return(
        <Button
            onClick={()=> {
                dispatch(operationsCustomers.deleteCustomer(id));
                dispatch(operationsCustomers.getCustomers())
            }}
        >DELETE CUSTOMER
        </Button>
    )
}
export default connect()(DeleteCustomer);
