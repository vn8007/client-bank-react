import axios from "axios";
import actionAccounts from "./actionAccounts";

const getAccounts = () => (dispatch) => {
    axios.get(`http://localhost:9000/accounts`).then((res) => {
        dispatch(actionAccounts.setAccountsData(res.data));
        dispatch(actionAccounts.setAccountsLoading(false))
    });
}

export default {
    getAccounts,
}