const SET_ACCOUNTS_DATA = 'client-bank/accounts/SET_ACCOUNTS_DATA';
const SET_ACCOUNTS_LOADING = 'client-bank/accounts/SET_ACCOUNTS_LOADING';

export default {
    SET_ACCOUNTS_DATA,
    SET_ACCOUNTS_LOADING
}