import accountsReducer from './reducerAccounts';

export { default as accountsOperations } from './operationsAccounts';
export { default as accountsActions } from  './actionAccounts';

export default accountsReducer;