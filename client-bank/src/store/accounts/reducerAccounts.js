import types from './typesAccounts';

const initialState = {
    data: null,
    isLoading: false,
};

const accountsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_ACCOUNTS_DATA: {
            return {
                ...state, data: action.data
            };
        }
        case types.SET_ACCOUNTS_LOADING: {
            return {
                ...state, isLoading: action.data
            };
        }
        default: return state;
    }
};

export default accountsReducer;