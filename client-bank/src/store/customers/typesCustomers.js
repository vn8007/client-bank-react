const SET_CUSTOMERS_DATA = 'client-bank/customers/SET_CUSTOMERS_DATA';
const SET_CUSTOMERS_LOADING = 'client-bank/customers/SET_CUSTOMERS_LOADING';

export default {
  SET_CUSTOMERS_DATA,
  SET_CUSTOMERS_LOADING
}