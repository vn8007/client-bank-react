import axios from "axios";
import actionCustomers from "./actionCustomers";

const getCustomers = () => (dispatch) => {
  axios.get(`http://localhost:9000/customers`).then((res) => {
    dispatch(actionCustomers.setCustomersData(res.data));
    dispatch(actionCustomers.setCustomersLoading(false))
  });
}

export default {
  getCustomers,
}