import types from './typesCustomers';

const setCustomersData = (customersData) => ({
  type: types.SET_CUSTOMERS_DATA,
  data: customersData,
});

const setCustomersLoading =(isLoading) =>({
  type: types.SET_CUSTOMERS_LOADING,
  data: isLoading,
});
export default {
  setCustomersData,
  setCustomersLoading,
};