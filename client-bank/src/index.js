import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./store";
import Dashboard from "./components/Dashboard";
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
              <Dashboard />
           </BrowserRouter>
      </Provider>
  </React.StrictMode>
);

reportWebVitals();
